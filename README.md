Simple server client program which has following attributes:
 1. Creates a variable type called "user_t" which holds user name, surname, age and id.
 2. Gets input from the user via CLI in order to fill "user_t".
 3. Creates a child process that creates a socket and sends "user_t" data to the client-side.
 4. Receives the data on the client-side and writes it into a file.
Provides server-client connection by ipv4 protocol.Server has a command handler which can receive commands from client and respond them.Supported commands are set,get and save user.Client has a command line interface.
